package com.devcamp.api.j58jpahibernate.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.j58jpahibernate.model.CVoucher;
import com.devcamp.api.j58jpahibernate.repository.IVoucherRepository;

@CrossOrigin
@RestController
public class CVoucherController {
    @Autowired
    IVoucherRepository voucherRepository;

    @GetMapping("/vouchers")
    public ArrayList<CVoucher> getAllVouchers() {
        ArrayList<CVoucher> lstVouchers = new ArrayList<>();
        voucherRepository.findAll().forEach(lstVouchers::add);
        return lstVouchers;
    }

     @GetMapping("/voucher2")
    public ResponseEntity<ArrayList<CVoucher>> getAllVoucher2() {
        ArrayList<CVoucher> lstVouchers = new ArrayList<>();
        try {
            voucherRepository.findAll().forEach(lstVouchers::add);
            if (lstVouchers.size() > 0) {
                return new ResponseEntity<>(lstVouchers, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(lstVouchers, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            // TODO: handle exception
        }                
    }
}
