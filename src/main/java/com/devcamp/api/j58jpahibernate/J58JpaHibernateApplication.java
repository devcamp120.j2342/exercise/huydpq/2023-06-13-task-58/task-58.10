package com.devcamp.api.j58jpahibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J58JpaHibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(J58JpaHibernateApplication.class, args);
	}

}
